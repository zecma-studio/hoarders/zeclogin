Z## Zecma Login

### Este proyecto es para el core principal de Zecma (Editar esta parte porque se tiene que documentar)


* **Clients**: _Este directorio será usado para establecer conexiones con otros microservicios_
* **Controllers** _Este directorio será utilizado para los controladores_
* **Models** _Este directorio estará la logica del negocio_
* **Services** _Directorio usado para establecer los servicios que se estarán usando como el de la BD, correo electronico, etc._


#### To-do's
- [x] Definir carpetas
- [ ] Conexión con la BD (postgres)
- [ ] Envío de correo
- [x] Registro a un paso
- [ ] Registro de segundo paso
- [ ] Registro con Gmail
- [ ] Creacion de token de acceso
- [ ] Verificación de token de acceso
- [ ] Renovación de token de acceso
- [x] Login de usuario
- [ ] Soporte a más de un rol


#### Base de datos
- [ ] Creacion de BD en AWS
- [ ] Creacion de user (tabla)
- [ ] Creacion de Log (tabla)
- [ ] Creacion de rol (tabla)
- [ ] Creacion de permiso (tabla)


#### Deploy
- [ ] Creación de docker
- [ ] Prueba unitaria de login con google
- [ ] Prueba unitaria de login normal
- [ ] Prueba unitaria de registro
- [ ] CI para deploy
- [ ] Creación de server en AWS
- [ ] Primer deploy


## Run in docker

`
docker build -t zeclogin .
`

`zeclogin` is going to be  name of the image

`
docker run -d -p 8080:80 --name zecl1 zeclogin
`

*You can change the 8080 port for any you like*

`zecl1` is container's name 