﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Entities.Zeclogin;
using Entities;

using Zeclogin.Helpers;
using Zeclogin.Helpers.Request;
using Zeclogin.Models.Interfaces;
using Zeclogin.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Zeclogin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {

        private ICountryModel _countryModel;
        public CountryController(ICountryModel countryModel)
        {
            _countryModel = countryModel;
        }

        // GET: api/<CountryController>
        [HttpGet]
        public ActionResult<Response> List()
        {
            try
            {
                var countries = _countryModel.GetCountryList();
                return new Response { data = countries, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // GET api/<CountryController>/5
        [HttpGet("{id}")]
        public ActionResult<Response> Get(string id)
        {
            try
            {
                Country country = _countryModel.GetById(id);
                if(country==null)
                {
                    return NotFound(new Response { message= "Country not found",code = 404 });
                }
                return new Response { data = country, code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // POST api/<CountryController>
        [HttpPost]
        [Authorize(Roles =Role.Admin)]
        public ActionResult<Response> Post([FromBody] CreateCountryRequest request)
        {
            try
            {
                Country country = _countryModel.CreateCountry(request.Code, request.Name);
                if (country == null)
                {
                    return Conflict( new Response {message ="Country already registered", code = 401});
                }
                return new Response { data = country, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });


            }
        }

        /*// PUT api/<CountryController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }*/

        // DELETE api/<CountryController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = Role.Admin)]
        public ActionResult<Response> Delete(string id)
        {
            try
            {
                bool deleted = _countryModel.DeleteCountry(id);
                if (!deleted)
                {
                    return NotFound(new Response { message = "NOT_FOUND", code = 404 });
                }
                return new Response { message = "OK", code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }
    }
}
