﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zeclogin.Helpers;
using Helpers.Auth;
using Zeclogin.Helpers.Request;
using Zeclogin.Models;
using Zeclogin.Services;
using Entities.Zeclogin;
using Entities;
using Zeclogin.Models.Interfaces;

namespace Zeclogin.Controllers
{

    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserModel _userModel;
        public UserController( IUserModel userModel)
        {
            this._userModel = userModel;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ActionResult<Response> Post([FromBody] AuthenticateModel model)
        {
            var user = _userModel.Authenticate(model.Username, model.Password);
            if (user == null)
            {
                return NotFound(new Response { message = "Invalid username or password", code = 404 });
            }
            user.Token = JWTHelper.ConvertUserToJWT(user);
            return new Response { message = "", code = 200, data = user };
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult<Response> FirstStep([FromBody] Register register)
        {
            try
            {
                var user = _userModel.FirstStep(register.Email, register.Password);
                if (user == null)
                {//Usuario ya registrado
                    // Conflict(new {});
                    return Conflict(new Response { message = "Email already registered.", code = 409 }); 
                }
                return new Response { data = user, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        [AllowAnonymous]
        [HttpPost("finish-register")]
        public ActionResult<Response> FinishRegister([FromBody] FullRegister fullRegister)
        {
            //TODO: Implement bearer and get userId from and email from there
            try
            {
                var user = _userModel.LastStep(fullRegister.UserId ,fullRegister.Email, fullRegister.Username, fullRegister.FirstName, fullRegister.LastName, fullRegister.Country, fullRegister.Avatar);
                if(user == null)
                {
                    return NotFound( new Response { message = "Something went wrong", code = 404 });
                }
                //Encontró el usuario y actualizó
                return new Response { data = user, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        [AllowAnonymous]
        [HttpGet("hi")]
        public ActionResult<Response> Hi()
        {
            return NotFound(new Response { message = "Here we are", code = 200 });
            //return ;
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet("list")]
        public ActionResult<Response> List()
        {
            try
            {
                var users = _userModel.GetAll();
                return new Response { data= users, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });
            }
        }

        [AllowAnonymous]
        [HttpPost("forgotten-username")]
        public ActionResult<Response> RequestUsername(UserPasswordRequest request)
        {
            try
            {
                var users = _userModel.requestUsername(request.Email);
                return new Response { message="Sent succesful",code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }

        }

        [AllowAnonymous]
        [HttpPost("recover-password")]
        public ActionResult<Response> RecoverPassword(UserPasswordRequest request)
        {
            try
            {
                var users = _userModel.requestPassword(request.Email);
                return new Response { message = "Sent succesful", code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }

        }

        [AllowAnonymous]
        [HttpPost("change-password")]
        public ActionResult<Response> RecoverPassword(ChangePasswordRequest request)
        {
            try
            {
                var user = _userModel.changePassword(request.Token, request.Password);
                return new Response { message = "Sent succesful", code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }

        }

    }
}
