﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Entities.Zeclogin;
using Entities;
using Zeclogin.Helpers;
using Zeclogin.Helpers.Request;
using Zeclogin.Models.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Zeclogin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvatarController : ControllerBase
    {
        private readonly IAvatarModel _avatarModel;
        public AvatarController(IAvatarModel avatarModel)
        {
            _avatarModel = avatarModel;
                
        }
        // GET: api/<AvatarController>
        [HttpGet]
        public ActionResult<Response> Get()
        {
            try
            {
                var avatars = _avatarModel.GetAvatarList();
                return new Response { data = avatars, code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // GET api/<AvatarController>/5
        [HttpGet("{id}")]
        public ActionResult<Response> Get(int id)
        {
            try
            {
                Avatar avatar = _avatarModel.GetById(id);
                if (avatar == null)
                {
                    return NotFound(new Response { code = 404 });

                }
                return new Response { data = avatar, code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // POST api/<AvatarController>
        [HttpPost]
        [Authorize(Roles = Role.Admin)]
        public ActionResult<Response> Post([FromBody] CreateAvatarRequest request)
        {
            try
            {
                Avatar avatar = _avatarModel.CreateAvatar(request.Name, request.Url);
                if(avatar == null)
                {
                    return Conflict(new Response { message = "Avatar already registered", code = 401 });

                }
                return new Response { data = avatar, code = 200 };
            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });

            }
        }

        // PUT api/<AvatarController>/5
       /* [HttpPut("{id}")]
        public Response Put(int id, [FromBody] string value)
        {
            try
            {

            }
            catch (Exception e)
            {
                return new Response { message = e.Message, code = -1 };
            }
        }*/

        // DELETE api/<AvatarController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = Role.Admin)]
        public ActionResult<Response> Delete(int id)
        {
            try
            {
                bool deleted = _avatarModel.DeleteAvatar(id);
                if (!deleted)
                {
                    return NotFound(new Response { message = "NOT_FOUND", code = 404 });
                }
                return new Response { message = "OK", code = 200 };

            }
            catch (Exception e)
            {
                return StatusCode(405, new Response { message = e.Message, code = -1 });
            }
        }
    }
}
