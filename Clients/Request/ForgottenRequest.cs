﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeclogin.Clients.Request
{
    public class ForgottenRequest
    {
        public string name { get; set; }
        public string to { get; set; }
        public string template { get; set; }
        public dynamic data { get; set; }
    }
}
