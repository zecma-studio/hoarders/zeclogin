﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeclogin.Clients.Request
{
    public class EmailUsername
    {
        public string username { get; set; }
        public string email { get; set; }
        public string url { get; set; }
    }
}
