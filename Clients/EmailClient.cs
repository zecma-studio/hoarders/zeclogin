﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Zeclogin.Clients.Request;
using Entities.Zeclogin;
using Zeclogin.Helpers;
using Zeclogin.Services;

namespace Zeclogin.Clients
{
    public class EmailClient : IEmailService
    {

        private readonly ClientSettings _settings;
        private HttpClient client;
        public EmailClient(IOptions<ClientSettings> options)
        {
            _settings = options.Value;
            client = HttpClientFactory.Create();
            client.BaseAddress= new Uri(_settings.Zecmail);
        }

        public async Task<bool> sendMissingPassword(string email, string username, string link)
        {
            ForgottenRequest body = new ForgottenRequest() { to = email, name = username, data = new EmailUsername { email = email, username = username, url = link }, template = "NEW_PASSWORD" };
            //Generate link

            //TODO: Add headers
            var content = Helpers.RequestHelper.CreateHttpContent(body);
            var response = client.PostAsync("/email",content);

            return true;
        }

        public async Task<bool> sendMissingUsername(string email, string username)
        {
            ForgottenRequest body = new ForgottenRequest() { to = email, name = username ,data= new  EmailUsername{username =username}, template = "FORGOTTEN_USERNAME" };
            //TODO: Add headers
            var content = Helpers.RequestHelper.CreateHttpContent(body);
            var response = await client.PostAsync("/email", content);

            return true;
        }

        public async Task<bool> sendWelcomeEmail(string email)
        {
            throw new NotImplementedException();
        }
    }
}
