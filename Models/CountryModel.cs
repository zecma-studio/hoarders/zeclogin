﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;

using Zeclogin.Models.Interfaces;
using Zeclogin.Services;

namespace Zeclogin.Models
{
    public class CountryModel : ICountryModel
    {
        private ICountryService _countryService;

        public CountryModel(ICountryService countryService)
        {
            _countryService = countryService;
        }


        public Country CreateCountry(string code, string name)
        {
            Country country = _countryService.SaveCountry(code, name);
            return country;
        }

        public bool DeleteCountry(string code)
        {
            bool success = _countryService.DeleteCountry(code);
            return success;
        }

        public Country GetById(string code)
        {
            Country country = _countryService.GetCountry(code);
            return country;
        }

        public IEnumerable<Country> GetCountryList()
        {
            IEnumerable<Country> countries = _countryService.GetCountryList();
            return countries;
        }
    }
}
