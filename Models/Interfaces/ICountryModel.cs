﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;


namespace Zeclogin.Models.Interfaces
{
    public interface ICountryModel
    {

        Country GetById(string code);
        Country CreateCountry(string code, string name);
        bool DeleteCountry(string code);
        IEnumerable<Country> GetCountryList();
    }
}
