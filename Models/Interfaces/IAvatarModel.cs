﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;


namespace Zeclogin.Models.Interfaces
{
    public interface IAvatarModel
    {
        Avatar GetById(int id);
        Avatar CreateAvatar(string name, string url);
        bool DeleteAvatar(int id);
        IEnumerable<Avatar> GetAvatarList();
    }
}
