﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;

namespace Zeclogin.Models.Interfaces
{
    public interface IUserModel
    {

        //TODO: Add missing methods

        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User FirstStep(string email, string password);
        User LastStep(int id, string email, string username, string firstName, string lastName, string country, int avatarId);
        bool requestUsername(string email);
        bool requestPassword(string email);
        User changePassword(string token, string password);
    }
}
