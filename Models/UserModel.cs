﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Security.Claims;
using System.Threading.Tasks;
using Entities.Zeclogin;
using Zeclogin.Helpers;
using Helpers.Auth;
using Zeclogin.Models.Interfaces;
using Zeclogin.Services;

namespace Zeclogin.Models
{
    public class UserModel : IUserModel
    {
        private  IUserService _userService;
        private  IEmailService _emailService;

        public UserModel(IUserService userService, IEmailService emailService)
        {
            this._userService = userService;
            this._emailService= emailService;

        }

        public User Authenticate(string username, string password)
        {
            User user = _userService.Authenticate(username, password);
            return user.WithoutPassword();
        }

        public User changePassword(string token, string password)
        {
            //If (User)
            Dictionary<String,Claim> tokens = (Dictionary<string, Claim>)JWTHelper.CheckJWT(token);
            if(tokens!=null)
            {
                Dictionary<String, String> hashedPassword = PasswordHelper.EncryptPassword(password);
                User user = _userService.ChangePassword(Int32.Parse(tokens["userId"].Value), hashedPassword["salt"], hashedPassword["hash"]);
                if(user!=null)
                    return user;
            }    
            return null;
        }

        public User FirstStep(string email, string password)
        {
            Dictionary<String, String> hashedPassword = PasswordHelper.EncryptPassword(password);
            User user = _userService.FirstStep(email, hashedPassword["salt"], hashedPassword["hash"]);
            return user;
        }

        public IEnumerable<User> GetAll()
        {
            IEnumerable<User> users = _userService.GetAll();
            return users;
        }

        public User GetById(int id)
        {
            User user = _userService.GetById(id);
            return user;
        }

        public User LastStep(int id, string email, string username, string firstName, string lastName, string country, int avatarId)
        {
            User user = _userService.LastStep(id, email, username, firstName, lastName, country,avatarId);
            return user;
        }

        public bool requestPassword(string email)
        {
            try
            {
                //Search user
                User user = _userService.searchByEmail(email);
                //Send email if user exists
                if (user != null)
                {
                    string token = JWTHelper.ConvertUserToUrlJwt(user);
                    string link = "https://localhost:5001/hi?token="+token;
                    _emailService.sendMissingPassword(user.Email, user.Username,link);
                }//Return 
                return true;
            }
            catch (Exception e)
            {
                //TOdo: handle exception
                return false;
            }
        }

        public bool requestUsername(string email)
        {

            try
            {
                //Search user
                User user = _userService.searchByEmail(email);
                //Send email if user exists
                if (user != null)
                {
                    _emailService.sendMissingUsername(user.Email, user.Username);
                }//Return 
                return true;
            }
            catch(Exception e)
            {
                //TOdo: handle exception
                return false;
            }
            
        }
    }
}
