﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Entities.Zeclogin;

using Zeclogin.Models.Interfaces;
using Zeclogin.Services;

namespace Zeclogin.Models
{
    public class AvatarModel : IAvatarModel
    {
        private IAvatarService _avatarService;
        public AvatarModel(IAvatarService avatarService)
        {
            _avatarService = avatarService;
        }
        public Avatar CreateAvatar(string name, string url)
        {
            Avatar avatar = _avatarService.SaveAvatar(name, url);
            return avatar;
        }

        public bool DeleteAvatar(int id)
        {
            bool success = _avatarService.DeleteAvatar(id);
            return success;
        }

        public Avatar GetById(int id)
        {
            Avatar avatar = _avatarService.GetAvatar(id);
            return avatar;
        }

        public IEnumerable<Avatar> GetAvatarList()
        {
            IEnumerable<Avatar> avatars = _avatarService.GetAvatarList();
            return avatars;
        }
    }
}
