﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Entities.Zeclogin;

namespace Zeclogin.Helpers
{
    public static class DataSecurityMethods
    {

        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            if (users == null) return null;

            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            if (user == null) return null;
            user.Salt = null;
            user.Password = null;
            return user;
        }
    }
}
