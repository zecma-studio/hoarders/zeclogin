﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zeclogin.Helpers
{
    public class Response
    {
        public int code { get; set; }
        public dynamic data { get; set; }
        public string message { get; set; }
    }
}
