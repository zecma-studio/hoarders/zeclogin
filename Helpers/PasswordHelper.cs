﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Zeclogin.Helpers
{
    public class PasswordHelper
    {

        public static Dictionary<String, String> EncryptPassword(string password)
        {
            Dictionary<String, String> ts= new Dictionary<string, string>();
            //Generating a 128 bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using(var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
                
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password,
                salt,
                KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 512 / 8
                ));
            ts.Add("salt", Convert.ToBase64String(salt));
            ts.Add("hash", hashed);
            return ts;
        }
        public static bool checkPassword(string password, string hashedPass, string salt)
        {

            byte[] saltByte = Convert.FromBase64String(salt);
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password,
                saltByte,
                KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 512 / 8
                ));

            //Are the same
            return (hashed==hashedPass);
        }
    }
}
