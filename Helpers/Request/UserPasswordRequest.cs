﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Zeclogin.Helpers.Request
{
    public class UserPasswordRequest
    {
        [Required]
        public string Email { get; set; }
    }
}
