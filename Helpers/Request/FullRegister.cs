﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Zeclogin.Helpers.Request
{
    public class FullRegister
    {
        //TODO: Verify userId attribute
        [Required]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        public string Email { get; set; }
        [Required]
        public string Country { get; set; }
        public int Avatar { get; set; }

    }
}
