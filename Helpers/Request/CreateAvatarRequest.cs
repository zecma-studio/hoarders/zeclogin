﻿using System.ComponentModel.DataAnnotations;

namespace Zeclogin.Helpers.Request
{
    public class CreateAvatarRequest 
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Url { get; set; }
    }
}
