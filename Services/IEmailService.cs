﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeclogin.Services
{
    public interface IEmailService
    {
        Task<bool> sendWelcomeEmail(string email);
        Task<bool> sendMissingPassword(string email, string username, string link);
        Task<bool> sendMissingUsername(string email, string username);
    }
}
