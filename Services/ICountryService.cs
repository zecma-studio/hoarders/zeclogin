﻿using System.Collections.Generic;
using Entities.Zeclogin;


namespace Zeclogin.Services
{
    public interface ICountryService
    {
        IEnumerable<Country> GetCountryList();
        Country GetCountry(string code);
        Country  SaveCountry(string code, string name);
        //Return true successful
        bool DeleteCountry(string code);
    }
}
