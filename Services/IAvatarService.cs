﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;


namespace Zeclogin.Services
{
    public interface IAvatarService
    {
        IEnumerable<Avatar> GetAvatarList();
        Avatar GetAvatar(int id);
        Avatar SaveAvatar(string name, string url);
        //Return true successful
        bool DeleteAvatar(int id);
    }
}
