﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Entities.Zeclogin;
using Zeclogin.Helpers;
using Entities;


namespace Zeclogin.Services
{
    class UserMemoryService : IUserService
    {

        private static List<User> _users = new List<User>
        {
            new User { Id = 1, FirstName = "Admin", LastName = "User", Email = "example@example.com", Username = "admin", Password = "admin", Role = Role.Admin },
            new User { Id = 2, FirstName = "Host", LastName = "User",Email = "example@example.com", Username = "host", Password = "host", Role = Role.Host },
            new User { Id = 3, FirstName = "ModRifa", LastName = "User", Email = "example@example.com", Username = "modrifa", Password = "modrifa", Role = Role.ModRifa },
            new User { Id = 4, FirstName = "ModPart", LastName = "User", Email = "example@example.com", Username = "modpart", Password = "modpart", Role = Role.ModPart },
            new User { Id = 5, FirstName = "Normal", LastName = "User", Email = "example@example.com", Username = "user", Password = "user", Role = Role.User },
            new User { Id = 6, FirstName = "Support", LastName = "User", Email = "example@example.com", Username = "support", Password = "support", Role = Role.Supp }
        };

        private readonly AppSettings _appSettings;

        public UserMemoryService(IOptions<AppSettings> appSettings)
        {
            this._appSettings = appSettings.Value;
        }
        public User Authenticate(string username, string password)
        {
            User usuario = _users.Find(user => (user.Username == username || user.Email == username) && user.Password == password);
            return usuario;
        }

        public IEnumerable<User> GetAll()
        {
            return _users.ToList<User>();
        }

        public User GetById(int id)
        {
            throw new NotImplementedException();
        }

        public User FirstStep(string email, string password)
        {
            User userCreated = _users.Find(user => user.Email == email);
            if (userCreated != null)
                return null;
            else
            {
                Console.WriteLine("Yes");
                _users.Add(new User { Id = _users.Count+1, Email = email, Password = password });
                return _users[_users.Count - 1];
            }

        }

        public User LastStep(int id,string email, string username, string firstName, string lastName)
        {
            User userCreated = _users.Find(user => user.Email == email && user.Id== id);
            if (userCreated == null)
                return null;
            else
            {
                userCreated.Username = username;
                userCreated.FirstName = firstName;
                userCreated.LastName= lastName;
                return userCreated;
            }
        }

        public User searchByEmail(string email)
        {
            User user = _users.SingleOrDefault<User>(user => user.Email == email);
            return user;
        }

        public User ChangePassword(int id, string password)
        {
            throw new NotImplementedException();
        }

        public User FirstStep(string email, string hash, string password)
        {
            throw new NotImplementedException();
        }

        public User ChangePassword(int id, string hash, string password)
        {
            throw new NotImplementedException();
        }

        public User LastStep(int id, string email, string username, string firstName, string lastName, string country, int avatarId)
        {
            throw new NotImplementedException();
        }
    }
}
