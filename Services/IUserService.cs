﻿using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using System.Text;
using Entities.Zeclogin;

namespace Zeclogin.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User FirstStep(string email, string hash, string password);
        User LastStep(int id, string email, string username, string firstName, string lastName, string country, int avatarId);
        User searchByEmail(string email);
        User ChangePassword(int id, string hash, string password);
    }
}
