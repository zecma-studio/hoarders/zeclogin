﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;
using Zeclogin.Models;

namespace Zeclogin.Services.Postgres
{
    public class UserDBService : IUserService
    {

        private readonly ZLoginContext context;

        public UserDBService(ZLoginContext context)
        {
            this.context = context;
        }

        public User Authenticate(string username, string password)
        {


            User user= context.Users.SingleOrDefault(user => (user.Username == username || user.Email == username));
            if (user != null)
            {
                bool hashedPassword = Helpers.PasswordHelper.checkPassword(password, user.Password, user.Salt);
            }
            return user;
        }

        public User ChangePassword(int id, string salt, string password)
        {
            User user = context.Users.SingleOrDefault(user => user.Id == id);
            if (user != null)
            {
                user.Password = password;
                user.Salt = salt;
                context.SaveChanges();
            }
            return user;
        }

        public User FirstStep(string email, string salt, string password)
        {
            User userCreated = context.Users.SingleOrDefault(user => user.Email == email);
            if (userCreated != null)
                return null;
            else
            {
                Console.WriteLine("Yes");
                User user = new User { Email = email,Salt = salt, Password = password };
                context.Users.Add(user);
                context.SaveChanges();
                return user;
            }
        }

        public IEnumerable<User> GetAll()
        {
            return context.Users.ToList();
        }


        public User GetById(int id)
        {
            User userCreated = context.Users.SingleOrDefault(user => user.Id == id);
            return userCreated;
        }

        public User LastStep(int id, string email, string username, string firstName, string lastName, string countryCode, int avatarId)
        {
            User userCreated = context.Users.SingleOrDefault(user => user.Email == email && user.Id == id);
            if (userCreated == null)
                return null;
            else
            {
                Country country = context.Countries.FirstOrDefault(country => country.Code == countryCode);
                Avatar avatar = context.Avatars.FirstOrDefault(avatar => avatar.Id == avatarId);
                if (avatar == null || country == null)
                    return null;
                userCreated.Username = username;
                userCreated.FirstName = firstName;
                userCreated.LastName = lastName;
                userCreated.Avatar = avatar;
                userCreated.Country = country;
                context.SaveChanges();
                return userCreated;
            }
        }

        public User searchByEmail(string email)
        {
            User user = context.Users.SingleOrDefault(user => user.Email == email);
            return user;
        }
    }
}
