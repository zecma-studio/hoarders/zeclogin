﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Zeclogin;

using Zeclogin.Models;

namespace Zeclogin.Services.Postgres
{
    public class AvatarDBService : IAvatarService
    {
        private readonly ZLoginContext context;
        public AvatarDBService(ZLoginContext context)
        {
            this.context = context;
        }
        public bool DeleteAvatar(int id)
        {
            Avatar avatar = context.Avatars.FirstOrDefault(avatar => avatar.Id == id);
            if (avatar != null)
            {
                context.Avatars.Remove(avatar);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Avatar GetAvatar(int id)
        {
            Avatar avatar = context.Avatars.FirstOrDefault(avatar => avatar.Id == id);
            return avatar;
        }

        public IEnumerable<Avatar> GetAvatarList()
        {
            IEnumerable<Avatar> avatars = context.Avatars.ToList();
            return avatars;
        }

        public Avatar SaveAvatar(string name, string url)
        {
            Avatar avatar = context.Avatars.FirstOrDefault(avatar => avatar.Name == name || avatar.Url == url);
            if (avatar == null)
            {
                avatar = new Avatar { Name = name, Url = url };
                context.Avatars.Add(avatar);
                context.SaveChanges();
                return avatar;
            }
            return null;
        }
    }
}
