﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Zeclogin;

using Zeclogin.Models;

namespace Zeclogin.Services.Postgres
{
    public class CountryDBService : ICountryService
    {

        private readonly ZLoginContext context;

        public CountryDBService(ZLoginContext context)
        {
            this.context = context;
        }
        public bool DeleteCountry(string code)
        {
            //Change this for some more efficient way
            Country country = context.Countries.FirstOrDefault(country => country.Code == code);
            if (country != null)
            {
                context.Countries.Remove(country);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Country GetCountry(string code)
        {
            Country country = context.Countries.FirstOrDefault(country => country.Code == code);
            return country;
        }

        public IEnumerable<Country> GetCountryList()
        {
            IEnumerable<Country> countries = context.Countries.ToList();
            return countries;
        }

        public Country SaveCountry(string code, string name)
        {
            Country country = context.Countries.FirstOrDefault(country => country.Code == code);
            if (country == null)
            {
                country = new Country { Code = code, Name = name };
                context.Countries.Add(country);
                context.SaveChanges();
                return country;
            }
            return null;
        }

    }
}
