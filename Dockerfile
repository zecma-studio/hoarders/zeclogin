#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Zeclogin.csproj", ""]
COPY ["../Helpers/Helpers.csproj", "../Helpers/"]
COPY ["../Entities/Entities.csproj", "../Entities/"]
RUN dotnet restore "./Zeclogin.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "Zeclogin.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Zeclogin.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Zeclogin.dll"]